package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.StudentModel;
import com.example.service.StudentService;

@RunWith(SpringRunner.class) 
@SpringBootTest 
@Transactional
public class StudentServiceTests {
	
	@Autowired  
	StudentService service;
	
	@Test
	public void testSelectAllStudents() {     
		
		List<StudentModel> students = service.selectAllStudents();
		
		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 2, students.size());
		
	}
	
	@Test
	public void testSelectStudent() {
		StudentModel student = service.selectStudent("123");
		
		Assert.assertNotNull("Gagal = student null", student);
		Assert.assertEquals("Gagal - nama student tidak sesuai", "Handy Nastiti", student.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 2.7, student.getGpa(), 0.0);
	}
	
	@Test
	public void testCreateStudent() {     
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);  
		
		// Cek apakah student sudah ada  
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));     
		
		// Masukkan ke service  
		service.addStudent(student); 
	 
		// Cek apakah student berhasil dimasukkan  
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));       
	}
	
	@Test
	public void testUpdateStudent() {
		
		//mengambil student dengan NPM
		StudentModel student = service.selectStudent("124");
		
		//mengubah GPA / Nama
		student.setGpa(4.00);
		
		//Update student
		service.updateStudent(student);
		
		//mengambil object student yang sudah diubah
		student = service.selectStudent("124");
		
		//Cek apakah perubahan berhasil
		Assert.assertEquals("Perubahan tidak berhasil", 4.00, student.getGpa(), 0.0);
	}
	
	@Test
	public void testDeleteStudent() {
		
		//mengambil student dengan NPM
		StudentModel student = service.selectStudent("124");
		
		//Cek student tidak Null
		Assert.assertNotNull("Gagal = student null", student);
		
		//Delete student
		service.deleteStudent(student.getNpm());
		
		//mengambil kembali objek student yang sudah dihapus
		student = service.selectStudent("124");
		
		//Cek apakah student tersebut Null
		Assert.assertNull("Mahasiswa gagal dihapus", student);
		
	}

}
